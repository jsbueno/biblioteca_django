import pytest

from livros import views, models


def test_view_edit_book_dont_create_author_if_author_in_form(admin_client, book_data):
    assert len(models.Author.objects.all()) == 1

    response = admin_client.post("/livros/editar/", book_data)
    assert response.status_code == 302

    book = models.Book.objects.all()[0]
    assert book.title == "Reinações de Narizinho"
    assert len(models.Author.objects.all()) == 1

    # Verificar se o campo new_author é ignorado
    # se author estiver preenchido


def test_view_create_book_creates_new_author_if_author_blank(admin_client):
    book_data = {
        "title": "Reinações de Narizinho",
        "isbn": 1222,
        "physical_copies": 0,
        "new_author": "Nome do Autor",
    }
    numero_autores = len(models.Author.objects.all())
    response = admin_client.post("/livros/editar/", book_data)
    assert len(models.Author.objects.all()) > numero_autores
    author = models.Author.objects.all()[0]
    assert author.name == 'Nome do Autor'

def test_view_create_book_creates_physical_copies(admin_client):
    book_data = {
        "title": "Reinações de Narizinho",
        "isbn": 1222,
        "physical_copies": 1,
        "new_author": "Nome do Autor",
    }
    numero_copias = len(models.PhysicalCopy.objects.all())
    response = admin_client.post("/livros/editar/", book_data)
    copias = models.PhysicalCopy.objects.all()
    assert len(copias) > numero_copias
    book = models.Book.objects.first()
    assert book.copies[0].volume_id == 1

def test_view_create_book_errors_on_fewer_physical_copies():
    assert True


def test_view_create_book_errors_on_duplicate_isbn():
    assert True