import pytest

from livros import models

@pytest.mark.parametrize(
    "model",[
        models.Book,
        models.Author,
        models.PhysicalCopy,
        models.Borrow,
        models.Customer,
    ]
)
def test_models_are_declared(model):
    assert model


@pytest.mark.django_db
def test_can_create_author():
    author = models.Author(name="Monteiro Lobato")
    author.save()

    new_author = models.Author.objects.all()[0]
    assert new_author.name == "Monteiro Lobato"


@pytest.mark.django_db
def test_can_create_book(fixture_author):
    book = models.Book(
        title="Memórias da Emília",
        author=fixture_author,
        isbn=1,
    )
    book.save()

    new_book = models.Book.objects.all()[0]
    assert new_book.title == "Memórias da Emília"
    assert new_book.author.name == "Monteiro Lobato"
