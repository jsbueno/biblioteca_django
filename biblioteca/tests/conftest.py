import pytest

from livros import models

@pytest.fixture()
def fixture_author():
    author = models.Author(name="Monteiro Lobato")
    author.save()
    return author


@pytest.fixture
def book_data(fixture_author):
    data = {
        "title": "Reinações de Narizinho",
        "author": fixture_author.id,
        "isbn": 1222,
        "physical_copies": 0,
    }
    return data