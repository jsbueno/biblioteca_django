from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import auth
from livros import models
from livros import forms
from livros.models import Book



# Create your views here.
def index(request):
    return render(request, template_name="index.html")


def list_books(request):
    books = Book.objects.all()
    for book in books:
        book.url = reverse("view_book", kwargs={"id": book.id})
    context = {
        "objects": books,
        "object_type": "Livros",
        "create_url": reverse("create_book")
    }
    return render(request, "object_list.html", context)


def book(request, id):
    book = Book.objects.get(id=id)
    return render(request, "book.html", {"book": book})


@login_required
@permission_required("livros.add_book")
def edit_book(request, id=None):
    if request.POST:
        if id:
            book = Book.objects.get(id=id)
            form = forms.BookForm(request.POST, instance=book)
        else:
            form = forms.BookForm(request.POST)

        form.full_clean()
        if not form.data.get("author") and form.data.get("new_author"):
            author = models.Author(name=form.data["new_author"])
            author.save()
        else:
            author = None

        if form.is_valid():
            book = form.save(commit=False)
            if author:
                book.author = author
            book.save()
            if form.cleaned_data.get("physical_copies", 0) != 0:
                new_copies = form.cleaned_data["physical_copies"]
                current_copies = len(book.copies)
                if new_copies < current_copies:
                    form._errors.setdefault("physical_copies", []).append("Não é possível diminuir o número de cópias")
                    return render(request, "book_form.html", {"book": book, "form": form})
                elif new_copies == current_copies:
                    pass
                else:
                    for i in range(current_copies, form.cleaned_data["physical_copies"]):
                        copy = models.PhysicalCopy(book=book, volume_id=i + 1, status="ready")
                        copy.save()
            return redirect(reverse("view_book", kwargs={"id": book.id}))
        else:
            if not id:
                book = None
    else:
        if id is None:
            book = Book()
        else:
            book = Book.objects.get(id=id)
        form = forms.BookForm(instance=book, initial={"physical_copies": len(book.copies) if book and book.id else 1})
    return render(request, "book_form.html", {"book": book, "form": form})


@login_required
def logout(request):
    auth.logout(request)
    return redirect(reverse('list_books'))
