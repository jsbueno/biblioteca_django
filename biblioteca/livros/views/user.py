from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib import auth
from livros import models
from livros import forms
from livros.models import Book

from django.views.generic import (
    ListView, DetailView, FormView, UpdateView, CreateView
)
from django.contrib.auth.models import User as DjangoUser


class UserList(ListView):
    template_name="user_list.html"
    model = DjangoUser


class UserView(DetailView):
    template_name="user_detail.html"
    model = DjangoUser
    # queryset = Users.objects.all()


class UserModify(UpdateView, PermissionRequiredMixin):
    template_name="user_edit.html"
    form_class = forms.UserForm
    model = DjangoUser

    def form_valid(self, form):
        user = form.save()
        return redirect(reverse('view_user', kwargs={"pk": user.pk}))


class UserEdit(UserModify, UpdateView):
    permission_required="livros.add_user"


class UserCreate(UserModify, CreateView):
    permission_required="livros.update_user"

"""
def user(request, id):
    book = Book.objects.get(id=id)
    return render(request, "book.html", {"book": book})

@login_required
@permission_required("livros.add_customer")
def edit_user(request, id=None):
    if request.POST:
        if id:
            book = Book.objects.get(id=id)
            form = forms.BookForm(request.POST, instance=book)
        else:
            form = forms.BookForm(request.POST)
        if form.is_valid():
            book = form.save()
            return redirect(reverse("view_book", kwargs={"id": book.id}))
        else:
            if not id:
                book = None
    else:
        if id is None:
            book = Book()
        else:
            book = Book.objects.get(id=id)
        form = forms.BookForm(instance=book)
    return render(request, "book_form.html", {"book": book, "form": form})
)
"""
