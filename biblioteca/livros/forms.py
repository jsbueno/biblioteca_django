from django import forms
from django.core.validators import ValidationError, MinLengthValidator
from django.contrib.auth.models import User as DjangoUser

from .models import Author, Book

def title_validator(value):
    if value.isdigit():
        raise ValidationError("O título do livro não pode ter só dígitos")

class BookForm(forms.ModelForm):
    title = forms.CharField(
        label="Título", validators=[
            title_validator,
            MinLengthValidator(3),
        ])

    new_author = forms.CharField(
        label = "Novo autor",
        required = False
    )

    physical_copies = forms.IntegerField(label='Cópias: ', initial=1)
    class Meta:
        model = Book
        exclude = []

BookForm.base_fields["author"].required = False


class UserForm(forms.ModelForm):
    customer = forms.BooleanField(label="Usuário", required=False)
    librarian = forms.BooleanField(label="Bibliotecário", required=False)
    contact = forms.CharField(label="Contato", required=False)
    class Meta:
        model = DjangoUser
        fields = ["username", "first_name", "last_name", "email"]
