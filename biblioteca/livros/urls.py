from django.urls import path
from livros.views import books
from livros.views import user

urlpatterns = [
    path('', books.index, name="index"),
    path('ver/', books.list_books, name="list_books"),
    path('ver/<int:id>', books.book, name="view_book"),
    path(r'editar/', books.edit_book, name="create_book"),
    path(r'editar/<int:id>', books.edit_book, name="edit_book"),
    path('logout', books.logout, name="logout"),
    path('usuarios', user.UserList.as_view(), name="list_users"),
    path('usuarios/<int:pk>/', user.UserView.as_view(), name="view_user"),
    path('usuarios/new', user.UserCreate.as_view(), name="create_user"),
    path('usuarios/<int:pk>/edit', user.UserEdit.as_view(), name="edit_user")
]
