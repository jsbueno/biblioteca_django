from concurrent.futures.thread import ThreadPoolExecutor

from django.contrib import admin
from django.db import models as django_models
from . import models

for model_name in "Book Author PhysicalCopy".split():

    model = getattr(models, model_name)

    admin.site.register(model)





