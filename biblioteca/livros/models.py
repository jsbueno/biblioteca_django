from datetime import timedelta

from django.db import models

class TimeStamped:
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Book(TimeStamped, models.Model):
    title = models.CharField(max_length=500)
    isbn = models.CharField(max_length=50)
    author = models.ForeignKey("livros.Author", on_delete=models.DO_NOTHING, null=True)

    @property
    def copies(self):
        return self.physicalcopy_set(manager="objects").all()

    def __str__(self):
        return f"{self.title} from {self.author.name}"


class Author(models.Model):
    name = models.CharField(max_length=255)
    def __str__(self):
        return self.name


class PhysicalCopy(models.Model):
    book = models.ForeignKey("livros.Book", on_delete=models.DO_NOTHING)
    volume_id = models.IntegerField()
    status=models.CharField(
        max_length=100,
        choices=(
            ("ready", "ready"),
            ("borrowed", "borrowed"),
            ("returned", "returned"),
            ("inactive", "inactive"),
        )
    )
    def __str__(self):
        return f"{self.book} {self.volume_id}"

class Customer(models.Model):
    user = models.ForeignKey("auth.User", on_delete=models.DO_NOTHING)
    active = models.CharField(
        max_length=30,
        choices=(
            ("active", "Ativo"),
            ("suspended", "Suspenso"),
        )
    )
    contact = models.CharField(max_length=100)
    def __str__(self):
        return f"Usuário {self.user.name:40}"

class Borrow(models.Model):
    copy = models.ForeignKey("livros.PhysicalCopy", on_delete=models.DO_NOTHING)
    customer = models.ForeignKey("livros.Customer", on_delete=models.DO_NOTHING)

    date_taken = models.DateField()
    date_returned = models.DateField()

    borrow_estimated_length = models.IntegerField("Dias para o empréstimo", default=7)
    status=models.CharField(
        max_length=30,
        choices=(
            ("ongoing", "em andamento"),
            ("returned", "devolvido"),
            ("not_started", "ainda não foi feito"),
        ),
        db_index=True
    )

    @property
    def return_date(self):
        return self.date_taken + timedelta(days=self.borrow_estimated_length)

    def __str__(self):
        return f"\"{self.copy.book.title}\" a devolver em {self.return_date}"


# Create your models here.
